import Ember from 'ember';
import anime from 'npm:animejs';

export default Ember.Component.extend({
  payerSearchModalActive: false,

  didInsertElement() {
    Ember.run.later(this, function() {
      this.fadeIn('.request__row');
    }, 160);
  },

  fadeIn(target) {
    anime({
      targets: target,
      duration: 500,
      easing: 'easeOutExpo',
      opacity: {
        value: [0,1],
        duration: 800,
        easing: 'linear'
      },
      delay: function(t,i) {
        return i * 100;
      }
    });
  },

  actions: {
    triggerPayerSearchModal() {
      this.set('payerSearchModalActive', true);
    }
  }
});
