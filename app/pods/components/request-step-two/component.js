import Ember from 'ember';
import anime from 'npm:animejs';

export default Ember.Component.extend({
  classNames: ['request--step-two'],

  didInsertElement() {
    Ember.run.later(this, function() {
      this.fadeIn('.request__row');
    }, 240);
  },

  fadeIn(target) {
    anime({
      targets: target,
      duration: 500,
      easing: 'easeOutExpo',
      opacity: {
        value: [0,1],
        duration: 800
      },
      delay: function(t,i) {
        return i * 100;
      }
    });
  }
});
