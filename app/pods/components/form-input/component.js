import Ember from 'ember';

export default Ember.Component.extend({
  classNames: ['input'],
  classNameBindings: ['error:input--error'],

  actions: {
    secondAction() {
      this.get('secondAction')();
    }
  }
});
