import Ember from 'ember';
import anime from 'npm:animejs';

export default Ember.Component.extend({
  classNames: ['table__wrapper'],
  selectedTrip: {},
  openModal: false,

  didInsertElement() {
    this.animateUp('.table__row');
  },

  animateUp(target) {
    anime({
      targets: target,
      translateY: [40, 0],
      duration: 500,
      easing: 'easeOutExpo',
      opacity: {
        value: [0,1],
        duration: 250,
        easing: 'linear'
      },
      delay: function(t,i) {
        return i * 20;
      }
    });
  },

  actions: {
    triggerModal(trip) {
      this.setProperties({
        selectedTrip: trip,
        openModal: true
      });
    }
  }
});
