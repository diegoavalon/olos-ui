import Ember from 'ember';
import anime from 'npm:animejs';

export default Ember.Component.extend({
  classNames: ['simple-dropdown'],
  classNameBindings: ['isDropdownActive:simple-dropdown--active'],
  isDropdownActive: false,

  animateDown() {
    anime({
      targets: '.simple-dropdown__dropdown',
      translateY: [0, 40],
      duration: 500,
      easing: 'easeOutExpo',
      opacity: {
        value: [1,0],
        duration: 80,
        easing: 'linear'
      }
    });
  },

  animateUp() {
    anime({
      targets: '.simple-dropdown__dropdown',
      translateY: [40, 0],
      duration: 500,
      easing: 'easeOutExpo',
      opacity: {
        value: [0,1],
        duration: 250,
        easing: 'linear'
      }
    });
  },

  actions: {
    triggerDropdown() {
      let isDropdownActive = this.get('isDropdownActive');

      if (!isDropdownActive) {
        this.set('isDropdownActive', true);
        this.animateUp();
      } else {
        this.animateDown();
        
        Ember.run.later(this, function() {
          this.set('isDropdownActive', false);
        }, 200);
      }
    },

    closeDropdown() {
      this.animateDown();

      Ember.run.later(this, function() {
        this.set('isDropdownActive', false);
      }, 200);

    }
  }
});
