import Ember from 'ember';
// import anime from 'npm:animejs';

export default Ember.Component.extend({
  classNames: ['modal'],
  classNameBindings: ['isModalActive:modal--active'],
  isModalActive: false,

  didUpdateAttrs() {
    anime({
      targets: '.modal__body',
      translateY: [40, 0],
      duration: 500,
      easing: 'easeOutExpo',
      opacity: {
        value: [0,1],
        duration: 80,
        easing: 'linear'
      }
    });

    anime({
      targets: '.modal__overlay',
      opacity: [0, 1],
      duration: 750
    });
  },

  animateDown() {
    anime({
      targets: '.modal__body',
      translateY: [0, 40],
      duration: 500,
      easing: 'easeOutExpo',
      opacity: {
        value: [1,0],
        duration: 80,
        easing: 'linear'
      }
    });

    anime({
      targets: '.modal__overlay',
      opacity: [1, 0]
    })
  },

  actions: {
    closeModal() {
      this.animateDown();
      
      Ember.run.later(this, function() {
        this.set('isModalActive', false);
      }, 500);
    }
  }
});
