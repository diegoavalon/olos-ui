import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    checkRadioButton() {
      let radioButtonId = this.get('radioButtonName');
      let radioElement = document.getElementById(radioButtonId);
      let fakeRadio = radioElement.nextElementSibling.nextElementSibling;
      
      radioElement.checked = true;
      fakeRadio.classList.toggle('is-checked');
    }
  }
});
