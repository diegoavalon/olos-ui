import Ember from 'ember';

export default Ember.Route.extend({
  model(){
    return {
         "id": "636413434594859245",
         "configuration": {
            "versionId": 4,
            "facilityId": "1073673737",
            "facilityName": "Memorial Medical Center",
            "baseConfigs": [
               {
                  "sectionId": "facilityTripView",
                  "sectionConfig": [
                     {
                        "fieldId": "requestDate",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 64
                     },
                     {
                        "fieldId": "requestTime",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 65
                     },
                     {
                        "fieldId": "requestedBy",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 66
                     },
                     {
                        "fieldId": "pickupLocation",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 67
                     },
                     {
                        "fieldId": "pickupDate",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 68
                     },
                     {
                        "fieldId": "pickupTime",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 69
                     },
                     {
                        "fieldId": "destinationLocation",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 70
                     },
                     {
                        "fieldId": "transportStatus",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 71
                     },
                     {
                        "fieldId": "providerAssigned",
                        "isDisplayed": true,
                        "isMandatory": false,
                        "fieldOrder": 85
                     }
                  ]
               },
               {
                  "sectionId": "losinformation",
                  "sectionConfig": [
                     {
                        "fieldId": "los",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 1
                     }
                  ]
               },
               {
                  "sectionId": "patientinformation",
                  "sectionConfig": [
                     {
                        "fieldId": "patientFirstName",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 1
                     },
                     {
                        "fieldId": "patientLastName",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 2
                     },
                     {
                        "fieldId": "patientMiddleName",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 3
                     },
                     {
                        "fieldId": "patientDob",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 4
                     },
                     {
                        "fieldId": "patientSsn",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 5
                     },
                     {
                        "fieldId": "patientMrn",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 6
                     },
                     {
                        "fieldId": "patientMrnSrc",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 7
                     },
                     {
                        "fieldId": "patientVisitNum",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 8
                     },
                     {
                        "fieldId": "patientVisitSrc",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 9
                     },
                     {
                        "fieldId": "patientGender",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 10
                     },
                     {
                        "fieldId": "patientAddress",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 11
                     },
                     {
                        "fieldId": "facilityAddress",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 12
                     },
                     {
                        "fieldId": "mailingAddress",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 13
                     },
                     {
                        "fieldId": "billingAddress",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 14
                     },
                     {
                        "fieldId": "diagnosis",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 15
                     },
                     {
                        "fieldId": "guarantor",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 16
                     },
                     {
                        "fieldId": "payerType",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 17
                     },
                     {
                        "fieldId": "payerAddress",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 18
                     },
                     {
                        "fieldId": "referenceNumber",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 19
                     },
                     {
                        "fieldId": "payerName",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 20
                     },
                     {
                        "fieldId": "primaryInsurance",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 21
                     },
                     {
                        "fieldId": "primaryPolicyNum",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 21
                     },
                     {
                        "fieldId": "primaryGroupNum",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 22
                     },
                     {
                        "fieldId": "additionalInsurance",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 23
                     },
                     {
                        "fieldId": "additionalPolicyNum",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 24
                     },
                     {
                        "fieldId": "additionalAuthNum",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 25
                     },
                     {
                        "fieldId": "additionalAuthNumNpi",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 26
                     },
                     {
                        "fieldId": "additionalGroupNum",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 27
                     },
                     {
                        "fieldId": "insurancyOrderofImportance",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 28
                     },
                     {
                        "fieldId": "dischargingPhysician",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 29
                     },
                     {
                        "fieldId": "receivingPhysician",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 30
                     },
                     {
                        "fieldId": "authorizingPhysicianNpi",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 31
                     },
                     {
                        "fieldId": "attendingPhysician",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 32
                     },
                     {
                        "fieldId": "consultingPhysician",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 33
                     },
                     {
                        "fieldId": "referringPhysician",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 34
                     },
                     {
                        "fieldId": "admittingPhysician",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 35
                     },
                     {
                        "fieldId": "over300Lbs",
                        "isDisplayed": true,
                        "isMandatory": false,
                        "fieldOrder": 72
                     },
                     {
                        "fieldId": "primaryAuthNum",
                        "isDisplayed": true,
                        "isMandatory": false,
                        "fieldOrder": 86
                     },
                     {
                        "fieldId": "authorizedBy",
                        "isDisplayed": true,
                        "isMandatory": false,
                        "fieldOrder": 87
                     },
                     {
                        "fieldId": "additionalAuthorizedBy",
                        "isDisplayed": true,
                        "isMandatory": false,
                        "fieldOrder": 88
                     },
                     {
                        "fieldId": "facilityPayOverride",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 89
                     },
                     {
                        "fieldId": "overrideReason",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 90
                     }
                  ]
               },
               {
                  "sectionId": "schedulingInformation",
                  "sectionConfig": [
                     {
                        "fieldId": "requestorType",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 36
                     },
                     {
                        "fieldId": "requestorName",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 37
                     },
                     {
                        "fieldId": "callbackNum",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 38
                     },
                     {
                        "fieldId": "perferredNotificationMethod",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 39
                     },
                     {
                        "fieldId": "primaryPhoneNum",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 40
                     },
                     {
                        "fieldId": "primaryPhoneType",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 41
                     },
                     {
                        "fieldId": "secondaryPhoneNum",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 42
                     },
                     {
                        "fieldId": "secondaryPhoneType",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 43
                     },
                     {
                        "fieldId": "requestingEntity",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 44
                     },
                     {
                        "fieldId": "department",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 45
                     },
                     {
                        "fieldId": "contract",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 46
                     },
                     {
                        "fieldId": "requestorRole",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 47
                     },
                     {
                        "fieldId": "pickupFacilityAddress",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 48
                     },
                     {
                        "fieldId": "pickupFacilityName",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 49
                     },
                     {
                        "fieldId": "pickupAddress",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 50
                     },
                     {
                        "fieldId": "pickupArea",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 51
                     },
                     {
                        "fieldId": "pickupLatitude",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 52
                     },
                     {
                        "fieldId": "pickupLongitude",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 53
                     },
                     {
                        "fieldId": "pickupFacilityTypeCode",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 54
                     },
                     {
                        "fieldId": "destinationFacilityName",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 55
                     },
                     {
                        "fieldId": "destinationAddress",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 56
                     },
                     {
                        "fieldId": "destinationDropOffArea",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 57
                     },
                     {
                        "fieldId": "destinationLatitude",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 58
                     },
                     {
                        "fieldId": "destinationLongitude",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 59
                     },
                     {
                        "fieldId": "destinationFacilityTypeCode",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 60
                     },
                     {
                        "fieldId": "pickupDate",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 61
                     },
                     {
                        "fieldId": "pickupTime",
                        "isDisplayed": true,
                        "isMandatory": true,
                        "fieldOrder": 62
                     },
                     {
                        "fieldId": "sla",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 63
                     },
                     {
                        "fieldId": "emailId",
                        "isDisplayed": true,
                        "isMandatory": false,
                        "fieldOrder": 73
                     },
                     {
                        "fieldId": "pickupComments",
                        "isDisplayed": true,
                        "isMandatory": false,
                        "fieldOrder": 74
                     },
                     {
                        "fieldId": "destinationBed/Apt/Room",
                        "isDisplayed": true,
                        "isMandatory": false,
                        "fieldOrder": 75
                     },
                     {
                        "fieldId": "destinationDepartment",
                        "isDisplayed": true,
                        "isMandatory": false,
                        "fieldOrder": 76
                     },
                     {
                        "fieldId": "destinationComments",
                        "isDisplayed": true,
                        "isMandatory": false,
                        "fieldOrder": 77
                     },
                     {
                        "fieldId": "additionalComments",
                        "isDisplayed": true,
                        "isMandatory": false,
                        "fieldOrder": 78
                     },
                     {
                        "fieldId": "identifyRecurring",
                        "isDisplayed": true,
                        "isMandatory": false,
                        "fieldOrder": 79
                     },
                     {
                        "fieldId": "identifyReturn",
                        "isDisplayed": true,
                        "isMandatory": false,
                        "fieldOrder": 80
                     },
                     {
                        "fieldId": "identifyNow",
                        "isDisplayed": true,
                        "isMandatory": false,
                        "fieldOrder": 81
                     },
                     {
                        "fieldId": "identifyWillCall",
                        "isDisplayed": true,
                        "isMandatory": false,
                        "fieldOrder": 82
                     },
                     {
                        "fieldId": "identifyRoundTrip",
                        "isDisplayed": true,
                        "isMandatory": false,
                        "fieldOrder": 83
                     },
                     {
                        "fieldId": "identifyOneWay",
                        "isDisplayed": true,
                        "isMandatory": false,
                        "fieldOrder": 84
                     },
                     {
                        "fieldId": "pickupFacBed/Apt/Room",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 100
                     },
                     {
                        "fieldId": "transportReason",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 101
                     },
                     {
                        "fieldId": "pickupDep",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldOrder": 102
                     }
                  ]
               }
            ],
            "overrideRules": [
               {
                  "overrideRuleId": 13,
                  "overrideElemType": "FIELDS",
                  "overridePriority": 1,
                  "overrideTriggerFields": [
                     {
                        "fieldId": "patientMrn",
                        "sectionId": "patientinformation",
                        "triggerValue": "not null"
                     }
                  ],
                  "overrideRuleLists": null
               },
               {
                  "overrideRuleId": 14,
                  "overrideElemType": "FIELDS",
                  "overridePriority": 1,
                  "overrideTriggerFields": [
                     {
                        "fieldId": "patientVisitNum",
                        "sectionId": "patientinformation",
                        "triggerValue": "not null"
                     }
                  ],
                  "overrideRuleLists": null
               }
            ],
            "overrideRuleConfigs": [
               {
                  "overrideRuleId": 13,
                  "overrideRules": [
                     {
                        "fieldId": "patientSsn",
                        "sectionId": "patientinformation",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldValue": null,
                        "fieldOrder": 4
                     }
                  ]
               },
               {
                  "overrideRuleId": 14,
                  "overrideRules": [
                     {
                        "fieldId": "patientSsn",
                        "sectionId": "patientinformation",
                        "isDisplayed": false,
                        "isMandatory": false,
                        "fieldValue": null,
                        "fieldOrder": 4
                     }
                  ]
               }
            ],
            "roleConfigs": [
               {
                  "roleId": 9,
                  "RoleDescription": "FacilityPayOverride",
                  "fieldId": "facilityPayOverride",
                  "sectionId": "patientinformation",
                  "isDisplayed": true,
                  "isMandatory": true
               },
               {
                  "roleId": 9,
                  "RoleDescription": "FacilityPayOverride",
                  "fieldId": "facilityPayOverrideReason",
                  "sectionId": "patientinformation",
                  "isDisplayed": true,
                  "isMandatory": true
               },
               {
                  "roleId": 10,
                  "RoleDescription": "LOSOverride",
                  "fieldId": "losOverride",
                  "sectionId": "losinformation",
                  "isDisplayed": true,
                  "isMandatory": true
               },
               {
                  "roleId": 10,
                  "RoleDescription": "LOSOverride",
                  "fieldId": "losOverrideReason",
                  "sectionId": "losinformation",
                  "isDisplayed": true,
                  "isMandatory": true
               }
            ]
         },
         "userDetails": {
            "id": 8,
            "username": "nikhila.podduturi@evhc.net",
            "firstName": "nikhila",
            "lastName": "podduturi",
            "emailAddress": "nikhila.podduturi@evhc.net",
            "isActive": true,
            "isTempPassword": false,
            "isAccountLocked": false,
            "lastLoggedIn": "2017-09-18T21:57:41.523",
            "tempPassCreateDate": "2017-09-08T16:26:28.76",
            "tempPassExpDate": "2017-09-08T16:26:28.76",
            "dept": null,
            "address": null,
            "city": null,
            "state": null,
            "zip": null,
            "managerName": null,
            "managerPhoneNumber": null,
            "managerEmailId": null,
            "modifiedDate": "2017-09-08T16:26:28.76",
            "modifiedBy": "rkallu",
            "isAuthenticated": true,
            "authenticationMsg": "User has been authenticated.",
            "isPasswordExpired": false,
            "lockedBy": null,
            "roles": [
               "Requester",
               "Approver",
               "SystemAdmin"
            ],
            "facilityIds": [
               {
                  "facilityId": 1073673737,
                  "facilityName": "Memorial Medical Center"
               }
            ],
            "securityQuestions": [
               {
                  "securityQuestion": "What high school did you attend?",
                  "securityAnswer": "abc",
                  "createdDate": "2017-09-18T18:46:28.413"
               },
               {
                  "securityQuestion": "In what city were you born?",
                  "securityAnswer": "abc",
                  "createdDate": "2017-09-18T18:46:28.51"
               },
               {
                  "securityQuestion": "What is your fathers middle name?",
                  "securityAnswer": "abc",
                  "createdDate": "2017-09-18T18:46:28.62"
               }
            ]
         },
         "lookups": {
            "ID": "2017-09-18T21:57:41.743",
            "los": [
               {
                  "id": "los-1",
                  "value": "TAXI",
                  "label": "Taxi Service"
               },
               {
                  "id": "los-2",
                  "value": "WC",
                  "label": "Wheel Chair Van"
               },
               {
                  "id": "los-3",
                  "value": "XPS",
                  "label": "Stretcher"
               },
               {
                  "id": "los-4",
                  "value": "BLS",
                  "label": "Basic Life Support Ambulance"
               },
               {
                  "id": "los-5",
                  "value": "ALS",
                  "label": "Advanced Life Support Ambulance"
               },
               {
                  "id": "los-6",
                  "value": "CCT",
                  "label": "Critical Care Transport"
               },
               {
                  "id": "los-8",
                  "value": "AMB",
                  "label": "AMBULANCE"
               },
               {
                  "id": "los-9",
                  "value": "NMS",
                  "label": "Non-Medical Stretcher"
               }
            ],
            "payerType": [
               {
                  "id": "payertype-1",
                  "label": "Medicaid",
                  "value": 1
               },
               {
                  "id": "payertype-2",
                  "label": "Medicare",
                  "value": 2
               },
               {
                  "id": "payertype-3",
                  "label": "Private Ins",
                  "value": 3
               },
               {
                  "id": "payertype-4",
                  "label": "Facility Pay",
                  "value": 4
               }
            ],
            "securityQuestions": [
               {
                  "id": "securityquestion-1",
                  "label": "In what city were you born?",
                  "value": 1
               },
               {
                  "id": "securityquestion-3",
                  "label": "What high school did you attend?",
                  "value": 3
               },
               {
                  "id": "securityquestion-4",
                  "label": "What is the first and last name of your first boyfriend or girlfriend?",
                  "value": 4
               },
               {
                  "id": "securityquestion-5",
                  "label": "What is the name of your favorite pet?",
                  "value": 5
               },
               {
                  "id": "securityquestion-6",
                  "label": "What is the name of your first grade teacher?",
                  "value": 6
               },
               {
                  "id": "securityquestion-7",
                  "label": "What is the name of your first school?",
                  "value": 7
               },
               {
                  "id": "securityquestion-8",
                  "label": "What is your fathers middle name?",
                  "value": 8
               },
               {
                  "id": "securityquestion-9",
                  "label": "What is your favorite color?",
                  "value": 9
               },
               {
                  "id": "securityquestion-10",
                  "label": "What is your favorite movie?",
                  "value": 10
               },
               {
                  "id": "securityquestion-11",
                  "label": "What is your mothers maiden name?",
                  "value": 11
               },
               {
                  "id": "securityquestion-12",
                  "label": "What street did you grow up on?",
                  "value": 12
               },
               {
                  "id": "securityquestion-13",
                  "label": "What was the make of your first car?",
                  "value": 13
               },
               {
                  "id": "securityquestion-14",
                  "label": "What was your favorite place to visit as a child?",
                  "value": 14
               },
               {
                  "id": "securityquestion-15",
                  "label": "What was your high school mascot?",
                  "value": 15
               },
               {
                  "id": "securityquestion-16",
                  "label": "When is your anniversary?",
                  "value": 16
               },
               {
                  "id": "securityquestion-17",
                  "label": "Which is your favorite web browser",
                  "value": 17
               },
               {
                  "id": "securityquestion-18",
                  "label": "Which phone number do you remember most from your childhood?",
                  "value": 18
               },
               {
                  "id": "securityquestion-19",
                  "label": "Who is your favorite actor, musician, or artist?",
                  "value": 19
               }
            ]
         },
         "operation": "authenticate",
         "isExpired": false,
         "username": "nikhila.podduturi@evhc.net"
      }
  }
});
