import Ember from 'ember';
import 'npm:smoothscroll-polyfill';

export default Ember.Controller.extend({
  list: [{
    value: 'Kaiser Permanente - Northern CA'
  }, {
    value: 'Kaiser Permanente - Southern CA'
  }],
  renderStepTwo: false,

  actions: {
    requestForm() {
      let elPosTop = document.getElementById('checkoutAnchor').offsetTop
      
      this.set('renderStepTwo', true);

      Ember.run.later(this, function() {
        window.scroll({ top: elPosTop, left: 0, behavior: 'smooth' });
      }, 80);
    }
  }
});
