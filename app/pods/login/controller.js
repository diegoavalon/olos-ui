import Ember from 'ember';

export default Ember.Controller.extend({
  loginState: true,
  registerState: false,
  resetPassword: false,

  actions: {
    goToLoginState() {
      this.setProperties({
        loginState: true,
        registerState: false,
        resetPassword: false
      });
    },

    goToRegisterState() {
      this.setProperties({
        loginState: false,
        registerState: true,
        resetPassword: false
      });
    },

    goToResetPasswordState() {
      this.setProperties({
        loginState: false,
        registerState: false,
        resetPassword: true
      });
    }
  }
});
