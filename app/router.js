import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('reports');
  this.route('help');
  this.route('request');
  this.route('login');
});

export default Router;
