(function() {
  function vendorModule() {
    'use strict';

    return {
      'default': self['animejs'],
      __esModule: true,
    };
  }

  define('animejs', [], vendorModule);
})();
